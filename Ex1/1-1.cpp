#include <iostream>
#include "Circle.h"
using namespace std;

int main()
{
   circle ob1;
   double area_earth, area_rope, hight_rope, clearance;

   ob1.set_rad(6378.1);
   area_earth = ob1.get_area(); 

   hight_rope = 0.001 + ob1.get_ference();
   ob1.set_ference(hight_rope);

   area_rope = ob1.get_area(); 
   clearance = area_rope - area_earth;

   cout << "Gap size = " << clearance << endl;

   return 0;
}
