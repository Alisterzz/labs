#ifndef _DateTime_
#define _DateTime_
#include <iostream>
using namespace std;

class DateTime{

public:
   DateTime(int day, int month, int year);
   DateTime();
   DateTime(const DateTime &ob);
   void printToday();
   void printMonth();
   void printWeekDay();
   void printYesterday();
   void printTomorrow();
   void printFuture(int n);
   void printPast(int n);
   friend void calcDifference(DateTime &a, DateTime &b);

private:
   int m_day, m_month, m_year;

};

#endif
