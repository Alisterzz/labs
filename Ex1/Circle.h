#ifndef _circle_
#define _circle_

#include <iostream>
#include <math.h>
using namespace std;

#define Pi 3.14

class circle{

public:

   void set_rad(double rad)
   {
      m_radius = rad;
      m_ference = 2*(m_radius*Pi); 
      m_area = Pi*(m_radius*m_radius);
   }

   void set_area(double ar)
   {
      m_area = ar;
      m_radius = sqrt(m_area/Pi);
      m_ference = 2*(m_radius*Pi);
   }

   void set_ference(double fer)
   {
      m_ference = fer;
      m_radius = (m_ference/Pi)/2;
      m_area = Pi*(m_radius*m_radius);
   }

   double get_rad(){ return m_radius; }

   double get_area(){ return m_area; }

   double get_ference(){ return m_ference; }

private:

   double m_radius, m_area, m_ference;

};
#endif

