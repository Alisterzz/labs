#include <iostream>
#include "DateTime.h"
using namespace std;

int main()
{
   DateTime ob1, ob2(10,03,2015);
   ob1.printToday();
   cout << "----------------" <<endl;
   ob1.printYesterday();
   cout << "----------------" <<endl;
   ob1.printTomorrow();
   cout << "----------------" <<endl;
   ob1.printFuture(33);
   cout << "----------------" <<endl;
   ob1.printPast(33);
   cout << "----------------" <<endl;
   calcDifference(ob1, ob2);

   return 0;
}
