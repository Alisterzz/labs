#include "DateTime.h"
#include <iostream>
#include <time.h>
using namespace std;

DateTime::DateTime(int day, int month, int year)
   {
      m_day = day;
      m_month = month-1;
      m_year = year-1900;
   }

DateTime::DateTime()
   {
      time_t t = time(&t);
      struct tm *p;
      p = localtime(&t);
      m_day = p ->tm_mday;
      m_month = p ->tm_mon;
      m_year =  p -> tm_year;
   }

DateTime::DateTime(const DateTime &ob)
   {
      m_day = ob.m_day;
      m_month = ob.m_month;
      m_year = ob.m_year;
   }

/*Current date*/
void DateTime::printToday()
   {
      cout << "The current date: " << endl;
      cout << "Number: " << m_day << endl;
      printWeekDay();
      printMonth();
      cout << "Year: " << m_year+1900 << endl;
   }

/*Month name*/
void DateTime::printMonth()
   {
      char * month[] = {"January", "February", "March", 
                       "April", "May", "June", "July", 
                       "August","September", "October", 
                       "November", "December"};
      cout << "Month : " << month[m_month]<<endl;
   }

/*Weekday name*/
void DateTime::printWeekDay()
   {
      char * day[] = {"Sunday", "Monday",
                      "Tuesday", "Wednesday", "Thursday",
                      "Friday", "Saturday"};
      time_t t = time(NULL);
      struct tm *curdate = localtime(&t);
      mktime(curdate);
      cout << "Weekday : " << day[curdate->tm_wday]<<endl;
   }

/*Yesterday*/
void DateTime::printYesterday()
   {
      time_t Today, Yesterday;
      time(&Today);
      Yesterday = Today - 24*3600;
      struct tm *yest = localtime(&Yesterday);
      cout << "Yesterday: " << asctime(yest) <<endl;
   }

/*Tomorrow*/
void DateTime::printTomorrow()
   {
      time_t Today, Tommorow;
      time(&Today);
      Tommorow = Today + 24*3600;
      struct tm *Tomm = localtime(&Tommorow);
      cout << "Tomorrow: " << asctime(Tomm) <<endl;
   }

/*Date after n days*/
void DateTime::printFuture(int n)
   {
      time_t Today, Future;
      time(&Today);
      Future = Today + (24*3600)*n; /*+n days*/
      struct tm *Fut = localtime(&Future);
      cout << "Date after "<< n <<" days: " << asctime(Fut) <<endl;
   }

/*Date n days ago*/
void DateTime::printPast(int n)
   {
      time_t Today, Past;
      time(&Today);
      Past = Today - (24*3600)*n; /* -n days */
      struct tm *Pas = localtime(&Past);
      cout << "Date "<< n <<" days ago: " << asctime(Pas) <<endl;
   }


void calcDifference(DateTime &a, DateTime &b)
{
   time_t result;
   struct tm one;
   one.tm_mday = a.m_day;
   one.tm_mon = a.m_month;
   one.tm_year = a.m_year;
   one.tm_hour = 0;
   one.tm_min = 0;
   one.tm_sec = 0;
   one.tm_wday = 0;

   struct tm two;
   two.tm_mday = b.m_day;
   two.tm_mon = b.m_month;
   two.tm_year = b.m_year;
   two.tm_hour = 0;
   two.tm_min = 0;
   two.tm_sec = 0;
   two.tm_wday = 0;

   result = (mktime(&one)-mktime(&two))  / (60*60*24);
   cout << "The result of subtracting two dates: " <<result << endl;
}


