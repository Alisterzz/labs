#include <iostream>
#include "PQueue.h"
using namespace std;

int main()
{
	PQueue ob;

	ob.PQinsert(10,1);
	ob.PQinsert(11,2);
	ob.PQinsert(12,7);
	ob.PQinsert(13,4);
	ob.PQinsert(14,5);
	ob.PQinsert(15,21);

	ob.show();
	ob.PQlenght();

	cout << "Max priority = " << ob.max_priority() << endl;

    ob.PQdelete_max_priority();

	cout << "*******************" << endl;
	ob.show();
	ob.PQlenght();

	cout << "Max priority = " << ob.max_priority() << endl;

	ob.PQclear();
	ob.show();

	return 0;
}