#include <iostream>
#include "PQueue.h"
using namespace std;

PQueue::~PQueue()
{
	PQueue *current = NULL;
	PQueue *next = m_first;

	while (next)
	{
		current = next;
		next = next->m_next;
		delete current;
	}
}

void PQueue::PQinsert(int item, int priority)
{
	PQueue *temp;
	PQueue *newList = m_first;
	
	if (m_first == NULL)
	{
		temp = new PQueue;
		temp ->m_item = item;
		temp ->m_priority = priority;
		temp ->m_next = m_first;
		m_first = temp;
	}
	else
	{
		temp = newList;
		while( temp ->m_next )
		{
			temp = temp ->m_next;
		}
		temp ->m_next = new PQueue;
		temp = temp ->m_next;
		temp ->m_item = item;
		temp ->m_priority = priority;
	}

	temp ->m_next = NULL;
	newList = temp;
	m_count++;
}

void PQueue::show()
{
	PQueue *newList = m_first; 
	if (m_first)
	{
		for (int i = 0; i < m_count; i++)
		{
			cout << "Item : " << newList->m_item; 
			cout << " , Pr : " << newList ->m_priority << endl;
			newList = newList->m_next;
		}
	}
	else cout << "The queue is empty!" << endl;
}

int PQueue::max_priority()
{
	PQueue *newList = m_first; 
	int max_prior = 0;

    for (int i = 0; i < m_count; i++)
	{
		if (newList->m_priority > max_prior)
		{
			max_prior = newList->m_priority;
		}
		newList = newList->m_next;
	}

	return max_prior;
}

void PQueue::PQdelete_max_priority()
{
	PQueue *previous = 0;
	PQueue *current = m_first;
	int flag = 1;
	while (flag)
	{
		if (current->m_priority == max_priority())
		{
			if (previous)
			{
				previous ->m_next = current ->m_next;
			}
			if (current == m_first)
			{
				m_first = current->m_next;
			}
			delete current;
			flag = 0;
			m_count--;
			cout << "Delete max priority..." << endl;
		}
		else
		{
			previous = current;
			current = current->m_next;
		}
	}

}

void PQueue::PQclear()
{
	PQueue *current = NULL;
	PQueue *next = m_first;
	while (next)
	{
		current = next;
		next = next->m_next;
		delete current;
	}
	m_first = NULL;
	m_count = 0;
	cout << "Cleaning is completed!" << endl;
}