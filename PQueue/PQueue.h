#ifndef _PQueue_
#define _PQueue_
#include <iostream>
using namespace std;

class list{
protected:
	int m_item, m_priority;
};

class PQueue: public list
{

private:
	int m_count; /*lenght*/
	PQueue *m_first;
	PQueue *m_next;

public:
	PQueue(){ m_count = 0; m_first = NULL;}
	PQueue(const PQueue &ob)
	{ 
		m_count = ob.m_count;
		m_first = ob.m_first;
		m_next = ob.m_next;
	}

	~PQueue();
	void PQlenght() { cout << "Lenght = " << m_count << endl; }
	void PQinsert(int item, int priority);
	void PQdelete_max_priority();
	void PQclear();
	int max_priority();
	void show();
};

#endif