#ifndef _PhoneBook_
#define _PhoneBook_
#include <iostream>
#include <fstream>
#include <map>
using namespace std;

class PhoneBook
{
public:
	void openbook();
	void insert();
	void show();
	void search(int phone);
	void search(string last_name);
	void clear_book();
	void Book_interface();

private:
	map <int, string> m_array;
	ifstream m_bok;
	ofstream m_book;
};

#endif 