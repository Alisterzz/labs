#include <iostream>
#include <map>
#include <fstream>
#include "PhoneBook.h"
#include <string>
using namespace std;

void PhoneBook::Book_interface()
{
	cout << "Hello! This is PhoneBook! " << endl;
	int enter;
	bool flag=true;

	while (flag)
	{
		cout << "********************" << endl;
		cout << "Select an action :" << endl;
		cout << "Read - 1" << endl;
		cout << "Write - 2" << endl;
		cout << "Search search by number - 3" << endl;
		cout << "Search by last name - 4" << endl;
		cout << "Exit - 0" << endl;
		cout << "Clear book - 911" << endl;
		cout << "********************" << endl;
		cin >> enter;
		
		switch (enter)
		{
		case 1: 
			{
				show(); 
				break;
			}
		case 2: 
			{
				insert(); 
				break;
			}
		case 3: 
			{
				int srch_number;
				cout << "********************" << endl;
				cout << "Enter the number to search" << endl;
				cin >> srch_number;
				search(srch_number); 
				break;
			}
		case 4: 
			{
				string srch_lname;
				cout << "********************" << endl;
				cout << "Enter the last name to search" << endl;
				cin >> srch_lname;
				search(srch_lname); 
				break;
			}
		case 911: 
			{
				clear_book(); 
				break;
			}
		case 0:
			{
				flag = false;
				break;
			}
	
		default: cout << "Input Error!" << endl;
		}
	}
}

void PhoneBook::clear_book()
{
	cout << "********************" << endl;
	cout << "Book cleared" << endl;
	m_book.open("PhoneBook.txt",ios_base::trunc);
	m_book.close();
	m_array.clear();
}


void PhoneBook::openbook()
{
	int phone;
	string last_name;

	m_bok.open("PhoneBook.txt",ios_base::in);
	while (!m_bok.eof())
	{
	m_bok >> phone;
	m_bok >> last_name;
	if (phone<=0) 
		phone=0;
	else
		m_array.insert(pair <int, string> (phone, last_name)); 
	}

	m_bok.close();
}
void PhoneBook::insert()
{
	bool flag = true;
	int phone;
	string last_name;
	string selection;

    m_book.open("PhoneBook.txt",ios_base::app);

	while (flag)
	{
		cout << "Write Phone number : " << endl;
		cin >> phone;
		cout << "Write Last name : " << endl;
		cin >> last_name;
		
		m_array.insert(pair <int, string> (phone, last_name)); 
		m_book << phone << " " << last_name << endl; 

		cout << "You have completed the entry ? Yes / no " << endl;
		cin >> selection;

		if (selection == "Yes" || selection == "yes" || selection == "y") 
			flag = false;
	}

    m_book.close();
}

void PhoneBook::show()
{
	cout << "********************" << endl;
	cout << "Data from the directory: " << endl;
	map <int, string>::iterator it;
	if (m_array.empty()) 
		cout << "The book is empty!" << endl;
	else
	{
		for ( it = m_array.begin(); it != m_array.end(); it++)
		{
			cout << (*it).first << " : " << (*it).second <<endl;
		}
	}
		
}

void PhoneBook::search(int phone)
{	
	map <int, string>::iterator it;
	int i = 0;
	cout << "Search: " << endl;

	for ( it = m_array.begin(); it != m_array.end(); it++) 
	{
		if ((*it).first==phone)
		{
			cout << (*it).first << " : " << (*it).second <<endl;
			i = 1;
		}
	}

	if (i==0) cout << "No search results ... " << endl;
}

void PhoneBook::search(string last_name)
{
	map <int, string>::iterator it;
	int i = 0;
	cout << "Search: " << endl;

	for ( it = m_array.begin(); it != m_array.end(); it++) 
	{
		if ((*it).second==last_name)
		{
			cout << (*it).first << " : " << (*it).second <<endl;
			i = 1;
		}
	}

	if (i==0) cout << "No search results ... " << endl;
}
