#ifndef _Automata_
#define _Automata_
#include <iostream>
using namespace std;

enum STATES {OFF, ON};

class Automata
{
public:
   Automata();
   void on();
   void off();
   void coin();
   void printMenu();
   void printState(enum STATES state);
   void choice();
   void check();
   void cancel();
   void cook();
   void finish();

private:
   int m_cash, m_choice;
   char *m_menu[4];
   int m_prices[4];
   enum STATES m_state;
   
};

#endif
