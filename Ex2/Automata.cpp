#include "Automata.h"
#include <iostream>
using namespace std;

Automata::Automata()
{
   m_menu[0] = "Coffee";
   m_menu[1] = "Cappuccino";
   m_menu[2] = "Americano";
   m_menu[3] = "Doppio";

   m_prices[0]=50;
   m_prices[1]=55;
   m_prices[2]=60;
   m_prices[3]=65;
   m_cash = 0;
   m_state = OFF;
   m_choice = 0;
}

void Automata::on()
{
   printState(ON); /*ON Automata*/
   printMenu();
}

void Automata::off()
{
   printState(OFF); /*OFF Automata*/
}

void Automata::printMenu()
{
   for(int i=0; i<4; i++)
      {
         cout << i << ": " << m_menu[i] << " : " << m_prices[i] << " rub" << endl;
      }
   cout << endl;
   choice();
}

void Automata::choice()
{

//Vvod nomera napitka
lable2:
   cout << "Select drink number: " << endl;
   cin >> m_choice;

   //Vihod s programmi
   if (m_choice == 911)
   {
      m_state = OFF;
      cancel();
   }
   else
   {

   //Proverka nabrannogo nomera 
   if (m_choice>3)
   {
      cout << "Entered wrong number..." << endl;
      cout << endl;
      goto lable2;
   }
   cout << endl;

   coin(); /*Perehod k vvodu deneg*/
   }
}

void Automata::coin()
{
   int choice;
   int money;
   
lable1:
   cout << "Please put the money into the machine!" << endl;
   cin >> money;

   cout << "You entered: " << money << "rub" << endl;
   m_cash = m_cash + money;

   cout << "Total: " << m_cash  << endl;
   cout << endl;

   cout << "You have finished entering the money? 0 - yes / 1 - no" << endl;
   cout << "If you want your money back, enter: 2 - Cancel" << endl;
   cin >> choice;

   switch (choice)
   {
   case 0:
	   {
              cout << "Total: " << m_cash << endl;
	      check(); /*Perehod k proverke deneg*/
	      break;
	   }

   case 1: 
	   {
	       goto lable1;
	       break;
	   }

   case 2:
	   { 
	      //Vozvrat deneg i vozvrashenie k nachalu
              cout << "Please take you money: " << m_cash << endl; 
              cout << endl;
              cout << "*****************************" << endl;
              cout << endl;
              cancel();
              break;
           }

   default: cout << "Error choice!" << endl;
   }

}

void Automata::check()
{
   if (m_prices[m_choice] <= m_cash)
      {     
         cout << "You change: " << 
         m_cash-m_prices[m_choice] << endl;
         cout << endl;
	 cook();
      }
   else
      {
         cout << "Not enough money..." << endl;
         cout << "Take you money: " << m_cash << endl;
	 cout << endl;
         cout << "*****************************" << endl;
         cout << endl;
         cancel();
      }
}

void Automata::printState(enum STATES state)
{
   switch(state)
   {
      case OFF: 
               {
                  cout << "Not working..." << endl;
                  m_state = OFF;
                  break;
               }

      case ON: 
               {
                  cout << "Waiting for action..." << endl;
                  m_state = ON;
                  break;
               }
      default: cout << "Error status!" << endl;
   }
}


void Automata::cancel()
{
	m_cash = 0;
	m_choice = 0;
	if (m_state == OFF) 
	{
		off();
	}
	if (m_state == ON) 
	{
		printMenu();
	}

}

void Automata::cook()
{
   cout << "Preparation of the drink..." << endl;
   cout << endl;
   finish();
}

void Automata::finish()
{
   cout << "Thank you for waiting, take you drink!" << endl;
   cout << endl;
   cout << "*****************************" << endl;
   cout << endl;
   cancel();
}
