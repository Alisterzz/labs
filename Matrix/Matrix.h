#ifndef _Matrix_
#define _Matrix_
#include <iostream>
#include <stdlib.h>

class Matrix
{
public:
	Matrix(int i, int j);
	Matrix() { p_matrix = 0; m_i = m_j = 0 ;}
	Matrix(const Matrix &ob);
	~Matrix();
	void show();
	void put();
	Matrix operator+(const Matrix &ob);
	Matrix operator-(const Matrix &ob);
	Matrix operator*(const Matrix &ob);
	Matrix operator/(const Matrix &ob);
	Matrix operator+(int a);
	Matrix operator-(int a);
	Matrix operator*(int a);
	Matrix operator/(int a);
	Matrix& operator=(const Matrix &ob);
	int operator==(Matrix &ob);
	int operator!=(Matrix &ob);
	double* operator[](int a) { return p_matrix[a]; }
	void transposition();
	double add(double **Matrix, int m_i, int m_j, int m_n, int m_m);
        double determ();
	void inverse(double det);


private:
	double **p_matrix;
	int m_i, m_j;
};

#endif 
