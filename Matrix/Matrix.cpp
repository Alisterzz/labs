#include <iostream>
#include <math.h>
#include "Matrix.h"
using namespace std;

Matrix::Matrix(int i, int j)
{

	m_i = i;
	m_j = j;
	int n, m;
	p_matrix = new double *[m_i];

	for(n = 0; n < m_i; n++)
	{
		p_matrix[n] = new double [m_j];
	}

	for(n = 0; n < m_i; n++)
	{
		for(m = 0; m < m_j; m++)
		{
			p_matrix[n][m] = 0;
		}
	}
}

Matrix::Matrix(const Matrix &ob)
{
	
	m_i = ob.m_i;
	m_j = ob.m_j;
	int n, m;
	p_matrix = new double *[m_i];

	for(n = 0; n < m_i; n++)
	{
		p_matrix[n] = new double [m_j];
	}

	for(n = 0; n < m_i; n++)
	{
		for(m = 0; m < m_j; m++)
		{
			p_matrix[n][m] = ob.p_matrix[n][m];
		}
	}
}

Matrix::~Matrix()
{
	 for(int i = 0; i < m_i; i++)
	 {
		 delete [] p_matrix[i];
	 }
	 delete [] p_matrix;
}

void Matrix::show()
{
	for (int i = 0; i < m_i; i++)
	{ 
		cout << endl;
		for (int j = 0; j < m_j; j++)
		{
			cout << p_matrix[i][j] << " ";
		}
	}
	cout << endl;
}

void Matrix::put()
{
	for (int i = 0; i < m_i; i++)
		for (int j = 0; j < m_j; j++)
		{
			p_matrix[i][j] = rand()%9+1;
		}
}

Matrix Matrix::operator+(const Matrix &ob)
{
	Matrix temp(ob);

	for (int i = 0; i < ob.m_i; i++)
		for (int j = 0; j < ob.m_j; j++)
			temp.p_matrix[i][j] = p_matrix[i][j] + ob.p_matrix[i][j];

	return temp;
}

Matrix Matrix::operator-(const Matrix &ob)
{
	Matrix temp(ob);

	for (int i = 0; i < ob.m_i; i++)
		for (int j = 0; j < ob.m_j; j++)
			temp.p_matrix[i][j] = p_matrix[i][j] - ob.p_matrix[i][j];

	return temp;
}

Matrix Matrix::operator*(const Matrix &ob)
{
	Matrix temp(ob);

	for (int i = 0; i < ob.m_i; i++)
		for (int j = 0; j < ob.m_j; j++)
			temp.p_matrix[i][j] = p_matrix[i][j] * ob.p_matrix[i][j];

	return temp;
}

Matrix Matrix::operator/(const Matrix &ob)
{
	Matrix temp(ob);

	for (int i = 0; i < ob.m_i; i++)
		for (int j = 0; j < ob.m_j; j++)
			temp.p_matrix[i][j] = p_matrix[i][j] / ob.p_matrix[i][j];

	return temp;
}

Matrix Matrix::operator+(int a)
{
	Matrix temp(m_i,m_j);
	temp.m_i = m_i;
	temp.m_j = m_j;
	int i, j;

	for (i = 0; i < temp.m_i; i++)
		for (j = 0; j < temp.m_j; j++)
			temp.p_matrix[i][j] = p_matrix[i][j] + a;

	return temp;
}

Matrix Matrix::operator-(int a)
{
	Matrix temp(m_i,m_j);
	temp.m_i = m_i;
	temp.m_j = m_j;
	int i, j;

	for (i = 0; i < temp.m_i; i++)
		for (j = 0; j < temp.m_j; j++)
			temp.p_matrix[i][j] = p_matrix[i][j] - a;

	return temp;
}

Matrix Matrix::operator*(int a)
{
	Matrix temp(m_i,m_j);
	temp.m_i = m_i;
	temp.m_j = m_j;
	int i, j;

	for (i = 0; i < temp.m_i; i++)
		for (j = 0; j < temp.m_j; j++)
			temp.p_matrix[i][j] = p_matrix[i][j] * a;

	return temp;
}

Matrix Matrix::operator/(int a)
{
	Matrix temp(m_i,m_j);
	temp.m_i = m_i;
	temp.m_j = m_j;
	int i, j;

	for (i = 0; i < temp.m_i; i++)
		for (j = 0; j < temp.m_j; j++)
			temp.p_matrix[i][j] = p_matrix[i][j] / a;

	return temp;
}

Matrix& Matrix::operator =(const Matrix &ob)
{
	int i, j;
	if(p_matrix)
	{
		for(i = 0; i < m_i; i++)
			delete []p_matrix[i];
		delete []p_matrix;
	}

	m_i = ob.m_i;
	m_j = ob.m_j;
	p_matrix = new double *[m_i];

	for (i = 0; i < m_i; i++)
	{
		p_matrix[i] = new double [m_j];
	}

	for (i = 0; i < m_i; i++)
		for (j = 0; j < m_j; j++)
			p_matrix[i][j] = ob.p_matrix[i][j];

	return *this;
}

int Matrix::operator==(Matrix &ob)
{
	return p_matrix==ob.p_matrix;
}

int Matrix::operator!=(Matrix &ob)
{
	return !(p_matrix==ob.p_matrix);
}

void Matrix::transposition()
{
	for(int i=0;i<m_i;i++)        
	{
		for(int j=0;j<m_j;j++)
            {
                p_matrix[i][j]=p_matrix[j][i];
            }
	}
        
}

double Matrix::add(double **Matrixs, int m_i, int m_j, int m_n, int m_m)
{
   int i,j,bi,bj;
   Matrix temp(m_i,m_j);
   for(i=0, bi=0; i<m_i; i++)
       {   
          if(i!=m_n)
          {   
             for(j=0, bj=0; j<m_j; j++)
		if(j!=m_m)
                {   
		   temp.p_matrix[bi][bj]=Matrixs[i][j];
		   bj++;
                }
	  bi++;
          }
       }
    
    if((m_n+m_m)%2)
        return temp.p_matrix[0][1]*temp.p_matrix[1][0] - temp.p_matrix[0][0]*temp.p_matrix[1][1];
    else
        return temp.p_matrix[0][0]*temp.p_matrix[1][1] - temp.p_matrix[0][1]*temp.p_matrix[1][0];
}

double Matrix::determ()
{
    int i;
    double sum;
    for(i=0, sum=0; i<m_i; i++)
        sum += p_matrix[i][0]*add(p_matrix, m_i, m_j, i, 0);
    
    return sum;
}

void Matrix::inverse(double det)
{
	if(det==0 || m_i!=m_j)
		cout << "Inverse impossible! " << endl;
	else
	{
		Matrix temp(m_i,m_j), current(m_i,m_j);
		int i,j;

		for(i=0; i<m_i; i++)
			for(j=0; j<m_j; j++)
				temp.p_matrix[i][j] = add(p_matrix, m_i, m_j, i, j) / det;
		
		for(i=0; i<m_i; i++)
			for(j=i+1; j<m_j; j++)
			{   
				current.p_matrix[i][j] = temp.p_matrix[i][j];
				temp.p_matrix[i][j] = temp.p_matrix[j][i];
				temp.p_matrix[j][i] = current.p_matrix[i][j];
			}
	
			cout << endl;
			cout << "Inverse Matrix: " << endl;
			temp.show();
	}
}
