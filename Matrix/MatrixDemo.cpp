#include <iostream>
#include "Matrix.h"
using namespace std;

int main()
{
	Matrix ob(3,3), ob2(3,5), ob3(3,3),ob4;
	ob.put();
	ob2.put();
	ob3.put();
	ob3.transposition();

	ob4=ob2+ob;
	ob4=ob2-ob;
	ob4=ob2*ob;
	ob4=ob2/ob;
	ob4 = ob+2;
	ob4 = ob-2;
	ob4 = ob*2;
	ob4 = ob/2;

	if (ob==ob2) cout << "ob == ob2" << endl;
	if (ob!=ob2) cout << "ob != ob2" << endl;

	cout << endl;
        cout << "Matrix 1: ";
	ob.show();
	cout << endl;

        cout << "Matrix 2: ";
	ob2.show();
        cout << endl;

        cout << endl;
        cout << "Matrix 3: ";
	ob3.show();
	cout << endl;

	cout << endl;
        cout << "Matrix 4: ";
	ob4.show();
	cout << endl;

	cout << "Determ Matrix 1 = " << ob.determ() << endl;
	ob.inverse(ob.determ());

	return 0;
}
