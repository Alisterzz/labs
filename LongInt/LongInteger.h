#ifndef _LongInteger_
#define _LongInteger_
#include <iostream>
using namespace std;

class LongInteger
{
public:
   LongInteger(int size );
   LongInteger() { m_size = 0; mp_array = 0; }
   ~LongInteger(){ delete [] mp_array; }
   LongInteger(const LongInteger &ob);
   LongInteger operator+ (const LongInteger &ob);
   LongInteger operator- (const LongInteger &ob);
   LongInteger operator* (const LongInteger &ob);
   LongInteger operator/ (const LongInteger &ob);
   LongInteger operator% (const LongInteger &ob);
   LongInteger& operator= (const LongInteger &ob);
   LongInteger operator^ (const LongInteger &ob);
   bool operator< (LongInteger &ob);
   bool operator> (LongInteger &ob);
   bool operator== (LongInteger &ob);
   bool operator!= (LongInteger &ob);
   long int& operator[](int i){ return mp_array[i]; }

private:
   long int *mp_array;
   int m_size ;
};

#endif
