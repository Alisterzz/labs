#include "LongInteger.h"
#include <cmath>
#include <iostream>
using namespace std;

LongInteger::LongInteger(int size)
{
   m_size = size;
   mp_array = new long int [m_size];

   for (int i = 0; i < m_size; i++)
      {
         mp_array[i] = 0;
      }
}

LongInteger::LongInteger(const LongInteger &ob)
{
   m_size = ob.m_size;
   mp_array = new long int [m_size];
}

LongInteger LongInteger::operator+(const LongInteger &ob)
{
   LongInteger temp(ob);

   for (int i = 0; i < ob.m_size; i++)
      {
         temp.mp_array[i] = mp_array[i] + ob.mp_array[i];
      }

return temp;
}

LongInteger LongInteger::operator-(const LongInteger &ob)
{
   LongInteger temp(ob);

   for (int i = 0; i < ob.m_size; i++)
      {
         temp.mp_array[i] = mp_array[i] - ob.mp_array[i];
      }

return temp;
}

LongInteger LongInteger::operator*(const LongInteger &ob)
{
   LongInteger temp(ob);

   for (int i = 0; i < ob.m_size; i++)
      {
         temp.mp_array[i] = mp_array[i] * ob.mp_array[i];
      }

return temp;
}

LongInteger LongInteger::operator/(const LongInteger &ob)
{
   LongInteger temp(ob);

   for (int i = 0; i < ob.m_size; i++)
      {
         temp.mp_array[i] = mp_array[i] / ob.mp_array[i];
      }

return temp;
}

LongInteger LongInteger::operator%(const LongInteger &ob)
{
   LongInteger temp(ob);

   for (int i = 0; i < ob.m_size; i++)
      {
         temp.mp_array[i] = mp_array[i] % ob.mp_array[i];
      }

return temp;
}

LongInteger LongInteger::operator^(const LongInteger &ob)
{
   LongInteger temp(ob);

   for (int i = 0; i < ob.m_size; i++)
      {
         temp.mp_array[i] = pow(mp_array[i],ob.mp_array[i]);
      }

return temp;
}

bool LongInteger::operator==(LongInteger &ob)
{
   return mp_array == ob.mp_array;
}

bool LongInteger::operator!=(LongInteger &ob)
{
   return !(mp_array == ob.mp_array);
}

bool LongInteger::operator<(LongInteger &ob)
{
   return mp_array < ob.mp_array;
}

bool LongInteger::operator>(LongInteger &ob)
{
   return mp_array > ob.mp_array;
}

LongInteger& LongInteger::operator= (const LongInteger &ob)
{
	if(mp_array)
	{
		for(int i = 0; i < m_size; i++)
			delete []mp_array;

	}
   m_size = ob.m_size; 
   mp_array = new long int [m_size];

   for (int i = 0; i < m_size; i++)
      {
         mp_array[i] = ob.mp_array[i];
      }
 
return *this;
}
